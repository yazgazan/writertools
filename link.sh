#!/usr/bin/env bash

dir="$(dirname "$0")"
installdir="/bin"
opts="$1"
prefix="$2"

if [[ "${dir[0]}" = "." ]]; then
  dir="$(pwd)/$dir"
fi

if [[ "$HOME" = "" ]]; then
  echo "Warning: couldn't find home directory. You should set your HOME env variable" > "/dev/stderr"
fi

if [[ ! "$prefix" = "" ]]; then
  installdir="$prefix"
elif [[ ! "$HOME" = "" ]] && [[ -d "$HOME/bin" ]]; then
  installdir="$HOME/bin"
elif [[ -d "/usr/local/bin" ]]; then
  installdir = "/usr/local/bin"
elif [[ -d "/usr/bin" ]]; then
  installdir = "/usr/bin"
fi

function installbin {
  sudo ln -sf $opts "$dir/bin/$1" "$installdir/"
  return $?
}

installbin notes/note
installbin notes/notes
installbin notes/rmnote
installbin books/books
installbin books/book_new
installbin books/book

echo "binaries installed in '$installdir'"

