#!/usr/bin/env bash

dir="$(dirname "$0")"

name="$1"
newname="$2"

if [[ "$name" = "" ]] || [[ "$newname" = "" ]]; then
  echo "Usage : $0 <name> <new name>" > "/dev/stderr"
  exit 2
fi

if [[ "$HOME" = "" ]]; then
  echo "Can't find home directory. please set the HOME env variable" > "/dev/stderr"
  exit 1
fi

. "$HOME/.writertools/global.sh"

if [[ ! -d "$BOOKPATH/$name" ]]; then
  nmatch="$(find "$BOOKPATH" -name "$name*" | wc -l)"
  if [[ "$nmatch" = "1" ]]; then
    name="$(basename "$(find "$BOOKPATH" -name "$name*")")"
  fi
fi

if [[ -f "$name" ]]; then
  /bin/mv "$name" "$newname"
  exit "$?"
fi
if [[ ! -d "$BOOKPATH/$name" ]]; then
  if [[ -d "$name" ]]; then
    /bin/mv "$name" "$newname"
    exit "$?"
  fi
  echo "Error : Couldn't find chapter '$name'" > "/dev/stderr"
  exit 1
fi

if [[ -d "$BOOKPATH/$newname" ]]; then
  echo "Error : chapter '$newname' already exists, couldn't rename" > "/dev/stderr"
  exit 1
fi

/bin/mv "$BOOKPATH/$name" "$BOOKPATH/$newname"

echo "'$name' -> '$newname'"

