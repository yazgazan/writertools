#!/usr/bin/env bash

dir="$(dirname "$0")"

if [[ "$HOME" = "" ]]; then
  echo "Can't find home directory. please set the HOME env variable" > "/dev/stderr"
  exit 1
fi

confdir="$HOME/.writertools"

mkdir -p "$confdir" || (echo "Can't create configuration dir. Check permissions on '$HOME'" > "/dev/stderr"; exit 1)

cat > "$confdir/global.sh" << EOF
#!/usr/bin/env bash

notedir="$HOME/notes"
writedir="$HOME/writings"

export SHELL=bash
EOF

cp -rf "$dir/bin/books/bin" "$confdir/"

chmod +x "$confdir/global.sh" || exit 2

. "$confdir/global.sh"

mkdir -p "$notedir" || exit 2
mkdir -p "$writedir" || exit 2

if [[ "$EDITOR" = "" ]]; then
  echo "Warning: you should set your EDITOR env variable for writertools to work propoerly" > "/dev/stderr"
fi

echo "Add '$dir/bin/notes' and '$dir/bin/books' to your path or run './link.sh'"

